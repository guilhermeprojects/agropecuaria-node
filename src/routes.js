const express = require('express');
const routes = express.Router();
const requireDir = require('require-dir');

const ProdutoController = require('./Controllers/ProdutoController');
const ClienteController = require('./Controllers/ClienteController');

routes.get('/produtos', ProdutoController.index);
routes.get('/produtos/:id', ProdutoController.show);
routes.post('/produtos', ProdutoController.store);
routes.put('/produtos/:id', ProdutoController.update);
routes.delete('/produtos/:id', ProdutoController.destroy);

routes.get('/clientes', ClienteController.index);
routes.get('/clientes/:id', ClienteController.show);
routes.post('/clientes', ClienteController.store);
routes.put('/clientes/:id', ClienteController.update);
routes.delete('/clientes/:id', ClienteController.destroy);

module.exports = routes;