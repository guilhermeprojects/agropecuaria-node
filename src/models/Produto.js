const mongoose = require('mongoose');

const ProdutoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    unidade_medida: {
        type: String,
        required: true,
    },
    qtde_minima: {
        type: String,
    },
    peso_produto: {
        type: String,
        required: true,
    },
    qtde: {
        type: String,
        required: true,
    },
    valor_unit: {
        type: String,
        required: true,
    },
    estoque: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
});

mongoose.model('Produto', ProdutoSchema);