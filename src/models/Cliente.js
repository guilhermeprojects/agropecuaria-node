const mongoose = require('mongoose');

const ClienteSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    tipo_cliente: {
        type: String,
        required: true,
    },
    situacao: {
        type: String,
		required: true,
    },
    email: {
        type: String,
        required: true,
    },
    telefone_comercial: {
        type: String,
        required: true,
    },
    celular: { 
        type: String,
        required: true,
    },
    site: {
        type: String,
    },
    tipo_endereco: {
        type: String,
        required: true,
    },
    cep: {
        type: String,
        required: true,
    },
    logradouro: {
        type: String,
    },
    numero: {
        type: String,
    },
	    complemento: {
        type: String,
    },
    bairro: {
        type: String,
    },
    cidade: {
        type: String,
        required: true,
    },
    uf: {
        type: String,
        required: true,
    },
    foto: {
        type: String,
    },
    observacao: {
        type: String,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
});

mongoose.model('Cliente', ClienteSchema);