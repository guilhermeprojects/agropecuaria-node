const mongoose = require('mongoose');

const Produto = mongoose.model('Produto');

module.exports = {
    async index(req, res) {
        const produtos = await Produto.find();

        return res.json(produtos);
    },

    async show(req, res) {
        const produtos = await Produto.findById(req.params.id);

        return res.json(produtos);
    },

    async store(req, res) {
        const produtos = await Produto.create(req.body);

        return res.json(produtos);
    },

    async update(req, res) {
        const produtos = await Produto.findByIdAndUpdate(req.params.id, req.body, {
            new: true
        });

        return res.json(produtos);
    },

    async destroy(req, res) {
        await Produto.findByIdAndRemove(req.params.id);

        return res.send();
    }
};
