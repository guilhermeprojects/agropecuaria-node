const mongoose = require('mongoose');

const Cliente = mongoose.model('Cliente');

module.exports = {
    async index(req, res) {
        const clientes = await Cliente.find();

        return res.json(clientes);
    },

    async show(req, res) {
        const clientes = await Cliente.findById(req.params.id);

        return res.json(clientes);
    },

    async store(req, res) {
        const clientes = await Cliente.create(req.body);

        return res.json(clientes);
    },

    async update(req, res) {
        const clientes = await Cliente.findByIdAndUpdate(req.params.id, req.body, {
            new: true
        });

        return res.json(clientes);
    },

    async destroy(req, res) {
        await Cliente.findByIdAndRemove(req.params.id);

        return res.send();
    }
};
